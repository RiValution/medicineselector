﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MedicineApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : Controller
    {
        private readonly DatabaseAPIContext _context;

        private readonly ILogger _logger;

        public EventController(DatabaseAPIContext context, ILogger<HomeController> logger)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Event>>> Get()
        {
            _logger.LogInformation("Got events log");
            return await _context.Event.ToListAsync();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Event even)
        {
            if (id != even.Id)
                return BadRequest();

            _context.Entry(even).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                _logger.LogError("Updating event-data error");
            }

            _logger.LogInformation("Updated event log");
            return NoContent();
        }
    }
}
