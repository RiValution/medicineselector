﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MedicineApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : Controller
    {
        private readonly DatabaseAPIContext _context;
        private readonly ILogger _logger;

        public HomeController(DatabaseAPIContext context, ILogger<HomeController> logger)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Appointment>> Get()
        {
            var list =  from g in _context.Appointment
                       where !_context.Event.Any(ev => (ev.AppointmentId == g.Id))
                       select g;
            _logger.LogInformation("Got appointments log");
            return list.ToList();
        }

        [HttpGet("{patientName}")]
        public ActionResult<List<Appointment>> GetName(string name)
        {
            var listPatients = _context.Patient.ToList();
            int patientId = (from g in listPatients where g.Name == name select g.Id).First();
            var patientAppointmentsList = from g in _context.Appointment
                       where  (g.PatientId == patientId) && (!_context.Event.Any(ev => (ev.AppointmentId == g.Id) ))
                       select g;

            if (patientAppointmentsList.ToList().Count==0)
            {
                _logger.LogInformation("Not found appointment by name log");
                return NotFound();
            }
            _logger.LogInformation("Got appointment by name log");
            return patientAppointmentsList.ToList();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Appointment appointm)
        {
            if (id != appointm.Id)
                return BadRequest();

            _context.Entry(appointm).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                _logger.LogError("Updating appointment-data error");
            }

            _logger.LogInformation("Updated appointment log");
            return NoContent();

        }
    }
}