﻿
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicineApplication
{
    public class DatabaseAPIContext : DbContext
    {
        public DbSet<Patient> Patient { get; set; }

        public DbSet<Appointment> Appointment { get; set; }

        public DbSet<Event> Event { get; set; }

        public DatabaseAPIContext(DbContextOptions<DatabaseAPIContext> options) : base(options)
        {}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=bin\\Debug\\netcoreapp3.1\\test.sqlite");
        }
    }
}
