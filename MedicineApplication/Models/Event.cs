﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MedicineApplication
{
    public class Event
    {
        public int Id { get; set; }

        [Required]
        [StringLength(60)]
        public string DateTime { get; set; }

        public int ServiceId { get; set; }

        public int? AppointmentId { get; set; }
    }
}
