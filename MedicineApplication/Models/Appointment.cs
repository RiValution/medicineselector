﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MedicineApplication
{
    public class Appointment
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public int ServiceId { get; set; }

        public Appointment() { }

        public Appointment(int id, int pateintId, int serviceId)
        {
            Id = id;
            PatientId = pateintId;
            ServiceId = serviceId;
        }
    }
}
